package com.threadjava.commentReactions;

import com.threadjava.commentReactions.model.CommentReaction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import java.util.Optional;
import java.util.UUID;

public interface CommentReactionsRepository extends CrudRepository<CommentReaction, UUID> {
    @Query("SELECT c " +
            "FROM CommentReaction c " +
            "WHERE c.user.id = :userId AND c.comment.id = :commentId ")
    Optional<CommentReaction> getCommentReaction(@Param("userId") UUID userId, @Param("commentId") UUID commentId);
}
