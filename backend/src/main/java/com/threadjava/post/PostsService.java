package com.threadjava.post;

import com.threadjava.comment.CommentRepository;
import com.threadjava.post.dto.*;
import com.threadjava.post.model.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.io.Console;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PostsService {
    @Autowired
    private PostsRepository postsCrudRepository;
    @Autowired
    private CommentRepository commentRepository;

    public List<PostListDto> getAllPosts(Integer from, Integer count, UUID userId, Boolean flag) {
        var pageable = PageRequest.of(from / count, count);
        if (!flag) {
            deletePostById(UUID.fromString("16105069-cb36-45ea-92d3-564fe0a353e6"));
            return postsCrudRepository
                    .findAllPosts(userId, pageable)
                    .stream()
                    .map(PostMapper.MAPPER::postListToPostListDto)
                    .collect(Collectors.toList());
        } else {
            return postsCrudRepository
                    .findAllPostsNotEqualUserId(userId, pageable)
                    .stream()
                    .map(PostMapper.MAPPER::postListToPostListDto)
                    .collect(Collectors.toList());
        }
    }

    public PostDetailsDto getPostById(UUID id) {
        var post = postsCrudRepository.findPostById(id)
                .map(PostMapper.MAPPER::postToPostDetailsDto)
                .orElseThrow();

        var comments = commentRepository.findAllByPostId(id)
                .stream()
                .map(PostMapper.MAPPER::commentToCommentDto)
                .collect(Collectors.toList());
        post.setComments(comments);
        return post;
    }

    public void deletePostById(UUID id) {
        Post post = postsCrudRepository.getOne(id);
        post.setDeleted((byte)1);
        postsCrudRepository.save(post);
    }

    public PostCreationResponseDto create(PostCreationDto postDto) {
        Post post = PostMapper.MAPPER.postDetailsDtoToPost(postDto);
        post.setDeleted((byte) 0);
        Post postCreated = postsCrudRepository.save(post);
        return PostMapper.MAPPER.postToPostCreationResponseDto(postCreated);
    }
}
