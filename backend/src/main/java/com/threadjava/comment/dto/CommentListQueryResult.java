package com.threadjava.comment.dto;

import com.threadjava.users.dto.UserShortDto;

import java.util.UUID;

public class CommentListQueryResult {
    private UUID id;
    private String body;
    public long likeCount;
    public long dislikeCount;
    private UserShortDto user;
}
