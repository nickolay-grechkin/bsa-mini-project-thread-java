import * as React from 'react';
import PropTypes from 'prop-types';
import { Button, Modal } from 'semantic-ui-react';

import { createAction } from '@reduxjs/toolkit';
import {
  post as postService
} from 'src/services/services';

const ActionType = {
  SET_ALL_POSTS: 'thread/set-all-posts'
};

const setPosts = createAction(ActionType.SET_ALL_POSTS, posts => ({
  payload: {
    posts
  }
}));

const deletePost = postId => async dispatch => {
  const posts = await postService.getAllWithoutFilterPosts();
  const index = posts.findIndex(x => x.postId === postId);
  posts.splice(index, 1);
  dispatch(setPosts(posts));
};

const DeletePost = ({ onPostDelete, postId, close }) => {
  const c = true;
  deletePost(postId);
  const handlePostDelete = () => onPostDelete(postId);
  return (
    <Modal open onClose={close}>
      <Modal.Header>
        Delete Your Account
      </Modal.Header>
      <Modal.Content>
        <p>
          Are you sure you want to delete post
          {postId}
          {c}
        </p>
      </Modal.Content>
      <Modal.Actions>
        <Button negative>
          No
        </Button>
        <Button positive onClick={handlePostDelete}>
          Yes
        </Button>
      </Modal.Actions>
    </Modal>
  );
};

DeletePost.propTypes = {
  postId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired,
  onPostDelete: PropTypes.func.isRequired

};

export default DeletePost;

