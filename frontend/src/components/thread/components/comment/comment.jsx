import * as React from 'react';
import { getFromNowTime } from 'src/helpers/helpers';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { Comment as CommentUI, Icon, Card, Label } from 'src/components/common/common';
import { commentType } from 'src/common/prop-types/prop-types';
import { IconName } from 'src/common/enums/enums';

import styles from './styles.module.scss';

const Comment = ({ comment: { body, createdAt, user } }) => (
  <CommentUI className={styles.comment}>
    <CommentUI.Avatar src={user.image?.link ?? DEFAULT_USER_AVATAR} />
    <CommentUI.Content>
      <CommentUI.Author as="a">{user.username}</CommentUI.Author>
      <CommentUI.Metadata>{getFromNowTime(createdAt)}</CommentUI.Metadata>
      <CommentUI.Text>{body}</CommentUI.Text>
      <Card.Content extra>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}

        >
          <Icon name={IconName.THUMBS_UP} />
          {9}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
        >
          <Icon name={IconName.THUMBS_DOWN} />
          {8}
        </Label>
      </Card.Content>
    </CommentUI.Content>
  </CommentUI>
);

Comment.propTypes = {
  comment: commentType.isRequired

};

export default Comment;
