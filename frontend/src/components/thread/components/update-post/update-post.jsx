import * as React from 'react';
import PropTypes from 'prop-types';
import { Button, Modal, Form } from 'semantic-ui-react';

const UpdatePost = ({ postId, close }) => {
  const c = true;
  return (
    <Modal open onClose={close}>
      <Modal.Header>
        Delete Your Account
        {postId}
        {c}
      </Modal.Header>
      <Modal.Content>
        <Form>
          <Form.TextArea
            Hello
            world
          />
        </Form>
      </Modal.Content>
      <Modal.Actions>
        <Button negative>
          No
        </Button>
        <Button positive>
          Yes
        </Button>
      </Modal.Actions>
    </Modal>
  );
};

UpdatePost.propTypes = {
  postId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired

};

export default UpdatePost;
